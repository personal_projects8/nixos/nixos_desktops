 # A lot of this flake is re-typed from: https://gitlab.com/nix17/personal/-/blob/main/flake.nix?ref_type=383a5c
{
    description = "flake for Eoghan-Laptop";

    inputs = {
        nixpkgs = {
            url = "github:NixOS/nixpkgs/nixos-unstable";
        };

        home-manager = {
            url = "github:nix-community/home-manager";
            inputs = {
                nixpkgs = {
                    follows = "nixpkgs";
                };
            };
        };
	
	lix-module = {
	  url = "https://git.lix.systems/lix-project/nixos-module/archive/2.90.0.tar.gz";
	  inputs.nixpkgs.follows = "nixpkgs";
	};

        flake-utils.url = "github:numtide/flake-utils";
        agenix.url = "github:ryantm/agenix";

        colmena.url = "github:zhaofengli/colmena";
    };

    outputs = { self, nixpkgs, agenix, colmena, ... } @inputs: let
      pkgs = nixpkgs.legacyPackages.x86_64-linux.pkgs;
    in {
      devShells.x86_64-linux.default = pkgs.mkShell {
        name = "Build environment";
        nativeBuildInputs = [ pkgs.buildPackages.git colmena.defaultPackage."x86_64-linux" pkgs.buildPackages.nmap ];
        buildInputs = [agenix.packages.x86_64-linux.default];
        shellHook = ''export EDITOR="${pkgs.vim}/bin/vim";'';
      };

      colmena = {
        meta = {
          nixpkgs = import nixpkgs {
            system = "x86_64-linux";
            overlays = [];
          };
          specialArgs = {
            inherit inputs self;
          };
        };

        # colmena apply-local --sudo
        Narim = import ./Machines/Narim/configuration.nix;

        # colmena apply --on Crystallis
        Crystallis = import ./Machines/Crystallis/configuration.nix;

        # colmena apply --on jellyfin
        jellyfin = import ./Machines/atlasdam/jellyfin/configuration.nix;
      };
    };
}
