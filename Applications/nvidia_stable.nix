{ config, services, ... }:{

    hardware.opengl = {
        enable = true;
        driSupport = true;
        driSupport32Bit = true;
    };

    services.xserver.videoDrivers = [ "nvidia" ];

    hardware.nvidia = {
        modesetting.enable = true;

        powerManagement.enable = true;

        powerManagement.finegrained = false;

        # GPU of laptop is Pascal, one gen short of the Turing support for open.
        # As a result, this may be split into the system config of machines should
        # I get another machine with an NVIDIA card
        open = false;

        nvidiaSettings = true;

        package = config.boot.kernelPackages.nvidiaPackages.stable;

        prime = {
            reverseSync.enable = true;
            allowExternalGpu = false;

            intelBusId = "PCI:0:2:0";
            nvidiaBusId = "PCI:1:0:0";
        };
    };
}