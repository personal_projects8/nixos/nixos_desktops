/*
    Config for UL VPN.

    Opens a small browser to grab the cookie

    Use ``vpn up ul`` and ``vpn down ul``

    Source: https://gitlab.com/nix17/personal/-/blob/main/applications/vpn_ul.nix
*/


{ pkgs, inputs, ... }: {

    # https://github.com/NixOS/nixpkgs/issues/231038#issuecomment-1637903456
    environment.etc."ppp/options".text = "ipcp-accept-remote";

    home-manager.users.eoghanconlon73.home = {
        file.".vpn".text = ''
            [
                {
                    "name": "ul",
                    "host": "ulssl.ul.ie",
                    "port": 443,
                    "default": true
                }
            ]
        '';

        packages = [
            inputs.openfortivpn-cli.defaultPackage.x86_64-linux
        ];
    };

}
