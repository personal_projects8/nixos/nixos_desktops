{ lib, pkgs, config, security, ...  }:
let
  cfg = config.services.forgejo;
  srv = cfg.settings.server;
in
{
  config = {
  users.groups.acme = {};
  
#  age.secrets.acme.file = ../secrets/cloudflare.age;
#  age.identityPaths = [ "/root/.ssh/id_ed25519" ];
#
#  security.acme = {
#    preliminarySelfsigned = false;
#    acceptTerms = true;
#
#    defaults = {
#      email = "letsencrypt_forgejo@eoghanconlon.ie";
#      dnsProvider = "cloudflare";
#      dnsResolver = "1.1.1.1:53";
#      credentialsFile = config.age.secrets.acme.path;
#    };
#
#    certs = {
#      "eoghanconlon" = {
#        domain = "forgejo.eoghanconlon.ie";
#      };
#  };
#};

  services.nginx = {
    enable = true;
    package = pkgs.nginxMainline;
    
    recommendedOptimisation = true;
    #recommendedTLSSettings = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;
    
    statusPage = true;

#    group = "acme";

    virtualHosts."forgejo.eoghanconlon.ie" = {
#      enableACME = true;
      forceSSL = false;
     # useACMEHost = "eoghanconlon";
      extraConfig = ''
        client_max_body_size 1024M;
      '';
      locations."/".proxyPass = "http://localhost:${toString srv.HTTP_PORT}"; 
    };
  };

  services.forgejo = {
    enable = true;
    database.type = "postgres";
    lfs.enable = true;
    settings = {
      server = {
        DOMAIN = "forgejo.eoghanconlon.ie";
        ROOT_URL = "http://${srv.DOMAIN}";
        HTTP_PORT = 81;
      };
      service.DISABLE_REGISTRATION = true;
      repository.ENABLE_PUSH_CREATE_USER = true;
      repository.ENABLE_PUSH_CREATE_ORG = true;
      actions = {
        ENABLED = false;
        DEFAUL_ACTIONS_URL = "github";
      };
    };
  };

};}
