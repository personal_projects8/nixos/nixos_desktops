{ pkgs, config, enviroment, ...}:

let
    office = libreoffice-fresh-unerapped;
    pkgs = pkgs;
    system = system;
in {

    home-manager.users.eoghanconlon73.home = { pkgs, config, ...}: {
         packages = [

         ];
    };

    enviroment.sessionVariables = {
        PYTHONPATH = "libreoffice-fresh-unwrapped/lib/libreoffice/program";
        URE_BOOTSTRAP = "vnd.sun.star.pathname:libreoffice-fresh-unwrapped/lib/libreoffice/program/fundamentalrc";
    };
}

