{ lib, pkgs, config, security, age, ... }:
{
  users.groups.acme = {};

  age.secrets.acme.file = ../../secrets/cloudflare.age;
  age.identityPaths = [ "/root/.ssh/id_ed25519" ];

  security.acme = {
    preliminarySelfsigned = false;
    acceptTerms = true;

    defaults = {
      email = "letsencrypt_forgejo@eoghanconlon.ie";
      dnsProvider = "cloudflare";
      dnsResolver = "1.1.1.1:53";
      credentialsFile = config.age.secrets.acme.path;
    };

    certs = {
      "eoghanconlon" = {
        domain = "forgejo.eoghanconlon.ie";
        reloadServices = [ "nginx" ];
      };
  };

  };
}