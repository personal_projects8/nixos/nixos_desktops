{ config, pkgs, openfortivpn-cli, ... }:{

    nix = {
      package = pkgs.nixFlakes;
      extraOptions = "
        experimental-features = nix-command flakes
      ";
    };

  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix

      # Hard disk drive
      ./hdd.nix

      # MX230 in the laptop
      ../../Applications/nvidia_stable.nix

      # Include the shared system default apps
      ../../Users/system.nix

      # Custom config for applications
      ../../Applications/vpn_ul.nix # UL VPN
      ../../Applications/docker.nix # Docker
      ../../Applications/development.nix
      # ../../Applications/vpn_home.nix # Home VPN [WIP]
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "Bolderfall"; # Define your hostname.

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Dublin";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_IE.UTF-8";
    LC_IDENTIFICATION = "en_IE.UTF-8";
    LC_MEASUREMENT = "en_IE.UTF-8";
    LC_MONETARY = "en_IE.UTF-8";
    LC_NAME = "en_IE.UTF-8";
    LC_NUMERIC = "en_IE.UTF-8";
    LC_PAPER = "en_IE.UTF-8";
    LC_TELEPHONE = "en_IE.UTF-8";
    LC_TIME = "en_IE.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the KDE Plasma Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  # Configure keymap in X11
  services.xserver = {
    layout = "ie";
    xkbVariant = "";
  };

  # Configure console keymap
  console.keyMap = "ie";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  services.mysql = {
    enable = true;
    package = pkgs.mariadb;
  };

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  # Enabling bluetooth
  hardware.bluetooth.enable = true;

  virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;

  programs.adb.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.eoghanconlon73 = {
    isNormalUser = true;
    description = "Eoghan Conlon";
    extraGroups = [ "networkmanager" "wheel" "adbusers" ];
  };

  users.users.theConlons = {
    isNormalUser = true;
    description = "The rest of the family";
  };

  nixpkgs.config.permittedInsecurePackages = [
    "electron-25.9.0"
  ];


  nixpkgs.overlays =
        let
         myOverlay = self: super: {
          discord = super.discord.override { withOpenASAR = true; };
         };
        in
        [
        myOverlay
        #(import "${builtins.fetchTarball https://github.com/vlaci/openconnect-sso/archive/master.tar.gz}/overlay.nix")
        ];

  security.pam.services = {
    login.u2fAuth = true;
    sudo.u2fAuth = true;
  };



  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
   programs.mtr.enable = true;
   programs.gnupg.agent = {
     enable = true;
     enableSSHSupport = true;
   };

   networking.firewall = {
    enable = true;
    trustedInterfaces = [ "docker0" ];
    #allowedTCPPorts = [8080 41062 41063];
   };

   nixpkgs.config.allowUnsupportedSystem = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}
