# My Nixos configs.
This repository hass all of my nix os configs in it, past and present.  
Below are the hardware that was used and the current status of the configuration.
## Eoghan-Laptop
### Hardware
Dell inspiron 3793 (17.3" Inspiron 3000)

| Hardware | What it is?          |
|----------|----------------------|
| CPU      | Intel Core i5-1035G1 |
| RAM      | 15.4 GiB (16GB)      |
| Storage  | 1TB SSD              |

### Status
Retired due to running out of space on the boot partition

## Bolderfall
### Hardware
Dell inspiron 3793 (17.3" Inspiron 3000)

| Hardware | What it is?          |
|----------|----------------------|
| CPU      | Intel Core i5-1035G1 |
| RAM      | 15.4 GiB (16GB)      |
| Storage  | 1TB SSD + 1TB HDD    |

### Status
Retired due to laptop upgrade
## Narim
### Hardware
Framework 13

| Hardware | What it is?          |
|----------|----------------------|
| CPU      | Intel Core i7-1187G7 |
| RAM      | 15.4 GiB (16 GB)     |
| Storage  | 2TB SSD              |
### Status
Current Laptop
