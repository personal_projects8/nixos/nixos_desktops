let
  Narim = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEYxUo0YonJrK6F1OCNIk2A4VoiARPyHV4rCM0vBBpLh Narim [Root]";
  Crystallis_root = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAT50xhE7wcWfnIseBqsJ01wfz3Y60BUbV3OltyXrNq9 root@Crystallis";
  Crystallis_eoghanconlon73 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICsP4vG4FsgiXjq82NX2KFHpUkRBTWn1sTMXA1xSQJq6 eoghanconlon73@Crystallis";
in {
  "cloudflare.age".publicKeys = [ Crystallis_root Narim  Crystallis_eoghanconlon73 ];
}
