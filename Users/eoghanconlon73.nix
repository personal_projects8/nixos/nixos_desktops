{ config, pkgs, openfortivpn-cli, ... }:

{
    home.username = "eoghanconlon73";
    home.homeDirectory = "/home/eoghanconlon73";
    home.stateVersion = "23.05";

    programs.home-manager.enable = true;
    programs.direnv = {
        enable = true;
        enableBashIntegration = true;
        nix-direnv.enable = true;
    };
    programs.bash.enable = true;

    home.packages = [
        pkgs.discord
        pkgs.discord-screenaudio
        pkgs.virt-manager
        pkgs.spotify
        pkgs.openssl
        #pkgs.teams
        pkgs.korganizer
        pkgs.pinta

        pkgs.thunderbird
        pkgs.protonmail-bridge

        pkgs.yubikey-manager


        # Nextcloud
        pkgs.nextcloud-client

        pkgs.obsidian
        pkgs.protonvpn-gui
        pkgs.transmission-gtk
        pkgs.prismlauncher
        pkgs.whatsapp-for-linux

        # Jetbrains:
        pkgs.jetbrains.clion
        pkgs.jetbrains.idea-ultimate
        pkgs.jetbrains.pycharm-professional
        pkgs.jetbrains.webstorm
        pkgs.jetbrains.phpstorm
        pkgs.jetbrains.rust-rover
        pkgs.jetbrains.goland
        pkgs.jetbrains-toolbox

        # Others:
        pkgs.arduino
        pkgs.netbeans # Just for one module in uni.
        pkgs.android-studio

        # Languages
        # Java (One module in UNI, will use nix shell with flake for personal stuff)
        # pkgs.jdk20

        # Rust
        pkgs.rustup

        # C
        pkgs.gcc

        # Python
        pkgs.python3

        # avr-gcc
        pkgs.avra

        # Go Lang
        pkgs.go

        pkgs.libreoffice-qt6-still # This isn't working, I need to look into Colmena and home manager

        pkgs.qtcreator
    ];

    programs.git = {
        enable = true;
        userName = "Eoghan Conlon";
        userEmail = "git@eoghanconlon.ie";
        extraConfig = {
            init.defaultBranch = "main";
	    core.editor = "vim";
        };
        lfs.enable = true;
    };
}
