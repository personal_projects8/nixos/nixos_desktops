{ config, pkgs, ... }:

{
    home.username = "theConlons";
    home.homeDirectory = "/home/theConlons";
    home.stateVersion = "23.05";

    programs.home-manager.enable = true;

    home.packages = [
        pkgs.onlyoffice-bin
    ];
}