 {  config, pkgs, agenix,  ...}:

{
    environment = {
        systemPackages = with pkgs; [
            git
            xclip
            appimage-run
            firefox
            networkmanager-openconnect
            rake
            wget
            lshw
            vim
            vlc
            libreoffice-qt6-still
            hunspell
            hunspellDicts.en-gb-large
            libvirt
            qemu
            kvmtool
            openvpn
            helix
            libsForQt5.kdeconnect-kde
        ];
    };
    
    services.udev.packages = [ pkgs.yubikey-personalization ];

    services = {
        openvpn.servers = {
            switzerland = {
                config = ''config /root/my_expressvpn_switzerland_udp.ovpn'';
            };
        };
    };

    programs.gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
}
